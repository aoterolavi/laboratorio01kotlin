package com.aotero.laboratorio01kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {

            val edad =edtEdad.text.toString().toInt()

            if(edad>=18){
                tvResultado.text="Usted es Mayor de Edad"
            }else{
                tvResultado.text="Usted es Menor de Edad"
            }
        }
    }
}